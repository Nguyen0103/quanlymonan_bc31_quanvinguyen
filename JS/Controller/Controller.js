export let foodController = {
    getFormInformation: () => {
        let foodName = document.querySelector('#foodName').value
        let foodPrice = document.querySelector('#foodPrice').value
        let foodDescription = document.querySelector('#foodDescription').value

        let food = {
            Name: foodName,
            Price: foodPrice,
            Description: foodDescription,
        }
        return food
    },

    showInformation: (foodDetail) => {
        document.querySelector('#foodName').value = foodDetail.Name
        document.querySelector('#foodPrice').value = foodDetail.Price
        document.querySelector('#foodDescription').value = foodDetail.Description
    }
}