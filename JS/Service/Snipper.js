export const snipper = {
    onLoading: () => {
        document.querySelector('#loading').style.display = "flex"
    },

    offLoading: () => {
        document.querySelector('#loading').style.display = "none"
    },
}