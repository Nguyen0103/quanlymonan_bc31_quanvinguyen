const BASE_URL = "https://62b0787ee460b79df0469c94.mockapi.io/mon-an";

export let foodService = {

    getFoodList: () => {
        return axios({
            url: BASE_URL,
            method: "GET",
        })
    },

    addFood: (food) => {
        return axios({
            url: BASE_URL,
            method: "POST",
            data: food,
        })
    },

    delFood: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "DELETE",
        })
    },

    getFoodDetail: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "GET",
        })
    },

    updateFood: (food) => {
        return axios({
            url: `${BASE_URL}/${food.Id}`,
            method: "PUT",
            data: food,
        })
    }
}