import { foodController } from "./Controller/Controller.js";
import { foodService } from "./Service/Service.js";
import { snipper } from "./Service/Snipper.js";

let foodList = [];

let foodEditer = null;

let renderTable = (list) => {
    let htmlContent = ""
    for (let i = 0; i < list.length; i++) {
        let food = list[i]
        htmlContent += `<tr>
                            <td>${food.Id}</td>
                            <td>${food.Name}</td>
                            <td>${food.Price}</td>
                            <td>${food.Description}</td>
                            <td>
                                <button type="button" class="btn btn-danger ml-1" onclick="delFood(${food.Id})">Xóa</button>
                                <button type="button" class="btn btn-primary mr-1" onclick="editFood(${food.Id})">Sửa</button>
                            </td>
                        </tr>`
    }
    document.getElementById('tbody-list').innerHTML = htmlContent
}

let renderServiceToTable = () => {

    snipper.onLoading()

    foodService.getFoodList()
        .then((res) => {
            snipper.offLoading()

            foodList = res.data;

            renderTable(foodList)
        })
        .catch((err) => {
            snipper.offLoading()
        });
}
renderServiceToTable()

let addFood = () => {
    snipper.onLoading()

    let foodInfo = foodController.getFormInformation()

    foodService.addFood(foodInfo)
        .then((res) => {
            snipper.offLoading()

            foodController.showInformation({
                Name: "",
                Price: "",
                Description: "",
            })

            renderServiceToTable()
        })
        .catch((err) => {
            snipper.offLoading()
        });
}
window.addFood = addFood;

let delFood = (id) => {

    foodService.delFood(id)
        .then((res) => {

            renderServiceToTable()
        })
        .catch((err) => {

        });
}
window.delFood = delFood;

let editFood = (id) => {
    foodEditer = id;

    snipper.onLoading()

    foodService.getFoodDetail(id)
        .then((res) => {
            snipper.offLoading()

            foodController.showInformation(res.data)
        })
        .catch((err) => {
            snipper.offLoading()
        });
}
window.editFood = editFood

let updateFood = () => {
    snipper.onLoading()

    let food = foodController.getFormInformation()

    let newFood = { ...food, Id: foodEditer }

    foodService.updateFood(newFood)
        .then((res) => {
            snipper.offLoading()

            foodController.showInformation({
                Name: "",
                Price: "",
                Description: "",
            })

            renderServiceToTable()
        })
        .catch((err) => {
            snipper.offLoading()
        });
}
window.updateFood = updateFood